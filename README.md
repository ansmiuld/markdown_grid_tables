# Markdown Grid Tables

[Python-Markdown](https://python-markdown.github.io/) [extension](https://python-markdown.github.io/extensions/api/) to support grid tables.

[Fork of Markdown-GridTables by Alexander Abbott](https://github.com/smartboyathome/Markdown-GridTables)
